#include <iostream>
#include <string>

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

using namespace sf;

int main(int argc, char **argv)
{
    ContextSettings settings;
    settings.antialiasingLevel = 0;

    Window window(VideoMode().getDesktopMode(), "App", Style::Default, settings);

    glClearColor(1.0f, 0.0f, 0.0f, 1.0f);

    Event event;
    while (window.isOpen())
    {
        while (window.pollEvent(event))
        {
            switch (event.type)
            {
            case Event::Closed:
                window.close();
                break;
            default:
                break;
            }
        }
        // Draw using openGL
        glClear(GL_COLOR_BUFFER_BIT);

        window.display();
    }

    return 0;
}